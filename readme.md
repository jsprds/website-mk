# Website voor MK Chiro Schelle #
Kleine onepage website voor de muziekkapel van Schelle (muziekkapel.chiroschelle.be). 

## Contribute ##
1. Fork repository
2. Use `$ gulp` in project folder to develop and `$ gulp build` to build a minified version
3. Send pull request
