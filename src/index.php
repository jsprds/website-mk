<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width">
	<title>Muziekkapel Chiro Schelle</title>
	<link rel="stylesheet" href="css/style.css">

	<!-- Standaard meta -->
	<meta name="description" content="De officiele website van de muziekkapel van Chiro Schelle. Boek ons nu en misschien spelen we binnenkort op uw evenement!">
	<meta name="keywords" content="muziekkapel, chiro, schelle, muziek, trompet, trommel, jachthoorn, trommelkorps, taptoe, optreden">

	<!-- Standaard open graph -->
	<meta property="og:title" content="Officiële website Muziekkapel Chiro Schelle">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Muziekkapel Chiro Schelle">
	<meta property="og:image" content="http://muziekkapel.chiroschelle.be/wp-content/themes/mk/img/muziekkapel-og.jpg" />
	<meta property="og:url" content="http://muziekkapel.chiroschelle.be">
	<meta property="og:description" content="De officiele website van de muziekkapel van Chiro Schelle. Vind hier alle informatie over onze muziekkapel!">
	<meta property="og:locale" content="nl_BE" />

	<!-- Twitter card -->
  	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@chiroschelle" />
	<meta name="twitter:image" content="http://muziekkapel.chiroschelle.be/wp-content/themes/mk/img/muziekkapel-og.jpg<?php echo '?'.uniqid(); ?>">

	<meta name="author" content="Jasper De Smet">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 month">
</head>
<body>
	<div class="sidebar">
		<div class="content">
			<h1>Muziekkapel <span>Chiro Schelle</span></h1>

			<p>
				De Muziekapel van Chiro Schelle is een jonge muziekgroep bestaande uit de verschillende afdelingstakken van de Chiro:
				Rakwi’s, Tito’s, Keti’s, Aspi’s, leiding en oudleiding.
				Met al onze leden maken we er super toffe repetities en optredens van.
				Het gehele ensemble bestaat uit natuurinstrumenten in Es.
				Ruiterijtrompetten, jachthoorns, bastrompetten, marstrommels, landsknechten en een grosse caisse.
				We spelen zowel stapmarsen, concertstukken als jazz getinte muziek.
			</p>

			<p>
				De muziekkapel op uw evenement? Dat kan zeker! Neem minsten één maand op voorhand contact op met <a href="mailto:muziekkapel@chiroschelle.be">muziekkapel@chiroschelle.be</a>
				en we beantwoorden uw aanvraag zo spoedig mogelijk.
			</p>
		</div>

		<ul class="links">
			<li><a href="http://chiroschelle.be">Chiro Schelle</a></li>
			<li><a href="http://facebook.com/MuziekkapelChiroSchelle">Facebook</a></li>
			<li><a href="https://www.youtube.com/results?search_query=muziekkapel+chiro+schelle">Youtube</a></li>
		</ul>

	</div>

	<?php
		// random image
		$i = rand(1, 3);
		$imageClass = 'image-' . $i;
	?>

	<div class="image <?php echo $imageClass; ?>">
		
	</div>

	<script>
	  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  	ga('create', 'UA-325339-9', 'auto');
	  	ga('send', 'pageview');
	</script>
</body>
</html>